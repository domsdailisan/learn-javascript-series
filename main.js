// alert("This is an alert")
/*
This is a
Multi line comment
*/

// console.log("This is a console log")
// console.log("DOms")

//Variables
//var, let, const
//let, const
// let age = 32
// console.log(age)
// age = 27
// console.log(age)

// const age = 32
// console.log(age)
// age = 27
// console.log(age)

// let age
// console.log(age)

//Data Tyes
//Primitive data types
//String, Number, Boolean, Null, Undefined, Symbol(ES6)

// const name = "Joe";
// const age = 32;
// const grade = 2.5;
// const isHot = true; //false
// const x = null 
// const y = undefined
// let z

//to check data type, use typeof
// console.log(typeof(name))
// console.log(typeof(age))
// console.log(typeof(grade))
// console.log(typeof(isHot))
// console.log(typeof(x))
// console.log(typeof(y))
// console.log(typeof(z))

//=================================

// const name = 'Joe'
// const age = 32

// console.log('My name is name and I am age yrs old')
// //String concatinations
// console.log('My name is ' + name + ' and I am ' + age + ' yrs old')

// const greetings = `My name is ${name} and I am ${age} yrs old`
// console.log(greetings)

// const string = 'Hello Spike!'
// console.log(string.length)
// console.log(string.toLowerCase())
// console.log(string.toUpperCase())
// console.log(string.substring(0, 7))
// console.log(string.substring(0, 7).toUpperCase())
// console.log(string.split(''))
// const subjects = 'math, science, english, filipino, recess'

// console.log(subjects.split(', '))
/**/
//Arrays

// const numbers = new Array(1,2,3,4,5)
// console.log(numbers)

// const fruits = ['mango', 'orange', 'apple', 'banana']
// console.log(fruits)
// const fruits = ['mango', 'orange', 'apple', 'banana', 1, 3, true]
// console.log(fruits)
// console.log(fruits[0]) //mango
// console.log(fruits[1]) //orange
// console.log(fruits[4]) //number
// console.log(fruits[6]) //boolean
// fruits[7] = 'grapes'
// console.log(fruits)
// // fruits = [] // re-assignment - mag eerror
// fruits.push('avocado') //push - adds item at the end
// console.log(fruits)

// fruits.unshift('Una') // unshift - adds item at the beginner
// console.log(fruits)

// fruits.pop() // pop() - remove last item
// console.log(fruits)

// fruits.shift() // shift() - remove first item
// console.log(fruits)

// console.log(Array.isArray(fruits)) //true
// console.log(Array.isArray('fruits')) //false

// console.log(fruits.indexOf('banana')) // 3

//Object Literals
//key : value
// const person = {
// 	firstName: 'John',
// 	lastName: 'Doe',
// 	age: 35,
// 	hobbies: ['eat', 'sleep', 'code'],
// 	address: {
// 		street: '#33 maligaya st.',
// 		city: 'Manila',
// 		region: 'NCR'
// 	}
// }

// console.log(person)
// // alert(person)
// console.log(person.firstName)
// console.log(person.hobbies)
// console.log(person.hobbies[1])
// console.log(person.address.city)

// const { firstName, lastName, age } = person

// console.log(firstName)
// console.log(lastName)
// console.log(age)

// person.color = 'kayumangi'

// console.log(person)

// const todos = [
// 	{
// 		id: 1,
// 		text: 'take a bath',
// 		isCompleted: false
// 	},
// 	{
// 		id: 2,
// 		text: 'Eat',
// 		isCompleted: true
// 	},
// 	{
// 		id: 3,
// 		text: 'toothbrush',
// 		isCompleted: false
// 	}
// ]

// console.log(todos[1].text)

// const todosJSON = JSON.stringify(todos)
// console.log(todosJSON)

//LOOPS

//FOR LOOP

// for (let i = 0; i <= 10; i++){
// 	//block of codes
// 	console.log(i)
// }

// //WHILE LOOP

// let i = 0
// console.log('WHILE LOOP')
// while(i <= 10) {
// 	//block of codes
// 	console.log(i)
// 	i++ // i = i + 1
// }

// const todos = [
// 	{
// 		id: 1,
// 		text: 'take a bath',
// 		isCompleted: false
// 	},
// 	{
// 		id: 2,
// 		text: 'Eat',
// 		isCompleted: true
// 	},
// 	{
// 		id: 3,
// 		text: 'toothbrush',
// 		isCompleted: false
// 	},
// 	{
// 		id: 4,
// 		text: 'code',
// 		isCompleted: true
// 	}
// ]

// //LOOP THROUGH ARRAY

// // for(let i = 0; i < todos.length; i++) {
// // 	//block codes
// // 	console.log(todos[i])
// // }
// // for(let i = 0; i < todos.length; i++) {
// // 	//block codes
// // 	console.log(todos[i].text)
// // }

// //FOR OF

// for(let todo of todos) {
// 	console.log(todo)
// 	console.log(todo.id)
// }

// //forEach, map, filter

// //forEach
// todos.forEach(function(todo){
// 	//block of codes
// 	console.log(todo.text)
// })

// //map

// const todosText = todos.map(function(todo){
// 	return todo.text
// })

// console.log(todosText)

// //filter

// const todosCompleted = todos.filter(function(todo){
// 	return todo.isCompleted === true
// }).map(function(todo){
// 	return todo.text
// })

// console.log(todosCompleted)

//CONDITIONALS

// const num = 13

// // if(num === 15) {
// // 	//block of codes
// // 	console.log("Num is equal to 15")
// // }

// //else statement

// // if(num === 15) {
// // 	//block of codes
// // 	console.log("Num is equal to 15")
// // }else {
// // 	console.log("Num is not equal to 15")
// // }

// //else if 

// if(num === 15) {
// 	//block of codes
// 	console.log("Num is equal to 15")
// }else if(num > 15){
// 	console.log("Num is greater than 15")
// }else {
// 	console.log("Num is lower than 15")
// }

//OR ||, AND &&


// const num1 = 5
// const num2 = 14

// if(num1 == 9 && num2 == 14) {
// 	console.log('BOTH conditions are true')
// }else if (num2 < 14 || num1 > 6) {
// 	console.log('at least ONE condition is true')
// }else {
// 	console.log('that is AND and OR operators')
// }

// if(num1 == 9) {
// 	if(num2 == 14) {
// 		console.log('BOTH conditions are true')
// 	}
// }

// //TERNARY OPERATOR

// const xx = 11
// const color = xx > 10 ? 'red' : 'blue'

// console.log(color)

// //<condition> ? <do this if true> : <do this if false>

// //SWITCH

// const newColor = 'yellow'

// switch(newColor) {
// 	case 'blue':
// 		console.log('color is blue')
// 		break
// 	case 'red':
// 		console.log('color is red')
// 		break
// 	default:
// 		console.log('no case is true')
// 		break
// }

//OOP

//Constructor function
// function Person(firstName, lastName, dob) {
// 	this.firstName = firstName
// 	this.lastName = lastName
// 	// this.dob = dob
// 	this.dob = new Date(dob)
// 	this.getBirthYear = function() {
// 		return this.dob.getFullYear()
// 	}
// 	this.getFullName = function() {
// 		return `${this.firstName} ${this.lastName}`
// 	}
// }

// function Person(firstName, lastName, dob) {
// 	this.firstName = firstName
// 	this.lastName = lastName
// 	this.dob = new Date(dob)
// }

// Person.prototype.getBirthYear = function() {
// 	return this.dob.getFullYear()
// }
// Person.prototype.getFullName = function() {
// 	return `${this.firstName} ${this.lastName}`
// }

// //instantiate object
// const person1 = new Person('John', 'Doe', '5-5-1985')

// console.log(person1)

// const person2 = new Person('Sarah', 'Doe', '5-5-1990')

// console.log(person2.firstName)

// console.log(person2.dob)
// console.log(person2.dob.getFullYear())

// console.log(person1.getBirthYear())

// console.log(person1.getFullName())
// console.log(person2.getFullName())

// console.log(person1)
// console.log(person1.getFullName())
// console.log(person2.getFullName())

// class Person {
// 	constructor(firstName, lastName, dob) {
// 		this.firstName = firstName
// 		this.lastName = lastName
// 		this.dob = new Date(dob)
// 	}

// 	getBirthYear() {
// 		return this.dob.getFullYear()
// 	}

// 	getFullName() {
// 		return `${this.firstName} ${this.lastName}`
// 	}
// }

// const person1 = new Person('John', 'Doe', '5-5-1985')
// const person2 = new Person('Sarah', 'Doe', '5-5-1990')

// console.log(person2.getFullName())
// console.log(person1)

//FUNCTIONS

// function addition(num1 = 10, num2 = 10) {
// 	// console.log('Hello')
// 	let sum = num1 + num2
// 	// console.log(sum)
// 	return sum
// }

// addition(10, 15)
// console.log(sum)
// console.log(addition(10, 15))
// console.log(addition())

// const addition = (num1 = 5, num2 = 3) => {
// 	return num1 + num2
// }
// const addition = (num1 = 5, num2 = 3) => console.log(num1 + num2)

// console.log(addition(10, 15))
// console.log(addition())

//DOM

// console.log(window)

// alert("hi!")

// window.alert("hello!")


//Single Element
// let form = document.getElementById('form')
// console.log(form)

// //querySelector
// console.log(document.querySelector('#form'))
// console.log(document.querySelector('.form-group'))

//Multiple Elements
//querySelectorAll
// console.log(document.querySelectorAll('.item'))

//getElementsByClassName
// console.log(document.getElementsByClassName('item'))

// let items = document.querySelectorAll('.item')
// items.forEach(item => console.log(item))

// let items = document.getElementsByClassName('item')
// items.forEach(item => console.log(item))

// let ul = document.querySelector('.items')

// ul.remove()
// ul.lastElementChild.remove()
// ul.firstElementChild.textContent = "First Item"

// ul.children[1].innerText = 'Second Item'

// ul.lastElementChild.innerHTML = '<h3>THIRD ITEM</h3>'

// let btn = document.querySelector('.button')
// btn.style.background = 'black'

// DOM - EVENTS

//
// let btn = document.querySelector('.button')

//eventListener
// btn.addEventListener('click', (e) => {
// 	e.preventDefault()
// 	console.log('button clicked!')
// 	console.log(e)
// 	console.log(e.target)
// 	console.log(e.target.className)
// 	console.log(e.target.value)
// })

// btn.addEventListener('click', (e) => {
// 	e.preventDefault()
// 	// document.querySelector('#form').style.background = `rgb(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}`
// 	// document.querySelector('body').classList.add('bgColor')
// 	// document.querySelector('body').classList.remove('bgColor')

// 	// document.querySelector('.items').lastElementChild.innerHTML = '<h1> Hi Doms! </h1>'
// 	//mouseover, mouseout
// })

// btn.addEventListener('mouseout', (e) => {
// 	e.preventDefault()
// 	document.querySelector('#form').style.background = `rgb(${Math.random()*255}, ${Math.random()*255}, ${Math.random()*255}`
// })

let form = document.querySelector('#form')
let username = document.querySelector('#username')
let email = document.querySelector('#email')
let password = document.querySelector('#password')
let confirm = document.querySelector('#confirm')
let regMsg = document.querySelector('#regMsg') //error message
let newReg = document.querySelector('#newReg')

form.addEventListener('submit', onSubmit)

function onSubmit(e) {
  e.preventDefault()

  // console.log(username)
  // console.log(username.value)

  if(username.value === '' || email.value === '' || password.value === '' || confirm.value === '') {
    regMsg.innerHTML ='Registration Fail <p> Invalid Input(s) </p>'
    regMsg.classList.add('redWarning')
  } else if(password.value !== confirm.value) {
    regMsg.innerHTML = 'Password did not match!!!'
    regMsg.classList.add('redWarning')
  } else {
    regMsg.innerHTML = 'Registration Successful!!!'
    regMsg.classList.add('greenSuccess')
  }

  let li = document.createElement('li')
  li.appendChild(document.createTextNode(`${username.value} : ${email.value}`))
  newReg.appendChild(li)

  username.value = ''
  email.value = ''
  password.value = ''
  confirm.value = ''
}

